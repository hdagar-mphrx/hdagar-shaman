/**
 * Created by Aayushi on 8/29/2016.
 */
var _ = require('lodash'),
    dashboardsTable = require('../pages/dashboards/table'),
    loginPage = require('../pages/login')
    dashboardsPage=require('../pages/dashboards/dashboards'),
    testData=require('./dashboards.data.json')


describe('Dashboards table', function() {
    var tabs;
    beforeAll(function () {
        loginPage.goLogin();
       loginPage.basePage.tab('Dashboards').then(function (dashboardsTab) {
            dashboardsTab.visit();
        });
    });

    it('should have the right heading', function () {
        expect(dashboardsTable.title).toEqual('Dashboards');
    });

    describe('Describe rows', function () {
        var row;
        beforeAll(function () {
            dashboardsTable.row(1).then(function (rowOne) {
                row = rowOne;
            });
        });
    });

    describe('Dashboard columns', function () {
        var columns;
        beforeAll(function () {
            dashboardsTable.columns.then(function (allColumns) {
                columns = allColumns;
            });
        });

        it('should have some columns', function () {
            expect(_.keys(columns)).toEqual(testData.columns);
        });

        it('should have description data', function () {
            var descriptionData = (testData.description);
            expect(columns['Description'].data()).toEqual(descriptionData);
        });

        it('should have type data', function () {
            var typeData = (testData.type);
            expect(columns['Type'].data()).toEqual(typeData);
        });

        it('should have user data', function () {
            var userData = (testData.user);
            expect(columns['User'].data()).toEqual(userData);
        });

        it('should have context data', function () {
            var contextData = (testData.context);
            expect(columns['Context'].data()).toEqual(contextData);
        });

        it('should have lastUpdatedBy data', function () {
            var lastUpdatedByData = (testData.lastUpdatedBy);
            expect(columns['Last updated by'].data()).toEqual(lastUpdatedByData);
        });

        it('should have actions', function () {
            var actionEditData = (testData.actionEdit);
            expect(columns['Actions'].data()).toEqual(actionEditData);
        });

       it('isSelectionContextDisplayed', function () {
            var elementsToBeDisplayed = ["selContext"];
            elementsToBeDisplayed.forEach(function (elementName) {
                expect(dashboardsPage[elementName].isDisplayed()).toBeTruthy();
            });
        });

        it('isTextKeywordDisplayed', function () {
            var elementsToBeDisplayed = ["txtKeyword"];
            elementsToBeDisplayed.forEach(function (elementName) {
                expect(dashboardsPage[elementName].isDisplayed()).toBeTruthy();
            })
        });

        it('isSelUserDisplayed', function () {
            var elementsToBeDisplayed = ["selUser"];
            elementsToBeDisplayed.forEach(function (elementName) {
                expect(dashboardsPage[elementName].isDisplayed()).toBeTruthy();
            })
        });

        it('keywordFilterSearch', function () {
            dashboardsPage.keywordSearchTxtBoxEnterTxt("test");
            var descriptionData = ['test_patient\'s PatientDashboard', 'test_internal\'s PatientOverview'];
            expect(columns['Description'].data()).toEqual(descriptionData);
            columns['Description'].data().then(function (promiseResolved) {
                console.log(promiseResolved);
            })
        });

        it('search By Multiple Criteria in dashboard listing', function () {
            dashboardsPage.contextFilterSelectValue("Patient Dashboard");
            dashboardsPage.keywordSearchTxtBoxEnterTxt("");
            dashboardsPage.keywordSearchTxtBoxEnterTxt("test");
            browser.waitForAngular();
            expect(dashboardsPage.getTotalResultsCount()).toEqual('1');
        });

        it('verify empty list for the dashboard listing page', function () {
            dashboardsPage.contextFilterSelectValue("Context");
            dashboardsPage.keywordSearchTxtBoxEnterTxt("");
            expect(dashboardsPage.getTotalResultsCount()).toEqual('12');
        });

        it('click on previous button when on first Page ', function () {
            dashboardsPage.keywordSearchTxtBoxEnterTxt("");
            expect(dashboardsPage.getCurrentResultsRange()).toEqual('1 - 10');
            expect(dashboardsPage.btnPreviousPage.isDisabled).toBeTruthy()
        });

        it('click on next button when on the last page ', function () {
            expect(dashboardsPage.getCurrentResultsRange()).toEqual('1 - 10');
            expect(dashboardsPage.getTotalResultsCount()).toEqual('12');
            dashboardsPage.getTotalResultsCount().then(function (promiseResolved) {
                console.log(promiseResolved);
            })
            dashboardsPage.gotoNextPage();
            expect(dashboardsPage.btnNextPage.isDisabled).toBeTruthy()
        });

        it('click on previous button when not on the first page ', function () {
            dashboardsPage.gotoPreviousPage();
            expect(dashboardsPage.getCurrentResultsRange()).toEqual('1 - 10');
            expect(dashboardsPage.btnPreviousPage.isDisabled).toBeTruthy()
        });

        it('verify click on edit dashboard button ', function () {
            dashboardsTable.row(1).then(function (rowTwo) {
                rowTwo.btnAction().click();
                browser.waitForAngular();
                expect(dashboardsPage.widgetRefreshBtn.isDisplayed).toBeTruthy();
            });
        });

        it('verify if add widget button is present ', function () {
            expect(dashboardsPage.widgetAddBtn.isDisplayed).toBeTruthy();
            expect(dashboardsPage.heartRateWidgetText.isDisplayed).toBeTruthy();
        });

        it('click on add widget button in edit dashboard page ', function () {
            dashboardsPage.widgetAddBtn.click();
            dashboardsPage.widgetGalleryCrossBtn.click();
            browser.waitForAngular();
        });

    });
        afterAll(function () {
            browser.waitForAngular();
            loginPage.basePage.logout();
        });

});










