/**
 * Created by aayushi on 9/15/2016.
 */

var _ = require('lodash'),
    dashboardsTable = require('../pages/dashboards/table'),
    basePage = require('../pages/base'),
    loginPage = require('../pages/login')
    dashboardsPage=require('../pages/dashboards/dashboards'),
    testData=require('./dashboards.data.json'),
    loginData=require('../Data/loginPage.spec.json'),


    describe('Dashboards table', function() {
        var tabs;
        beforeAll(function () {
            loginPage.goLogin(loginData.usernamePatient,loginData.passwordPatient);
                  basePage.tab('Dashboard').then(function (dashboardsTab) {
                dashboardsTab.visit();
            });
        });

        it('button for widget gallery', function () {
            browser.waitForAngular();
            expect(dashboardsPage.widgetAddBtnPatientDashboard.isDisplayed).toBeTruthy();
            expect(dashboardsPage.heartRateWidgetText.isDisplayed).toBeTruthy();
        });

        it('click widget gallery button action', function () {
            browser.waitForAngular();
            dashboardsPage.widgetAddBtnPatientDashboard.click();
            expect(dashboardsPage.widgetGalleryText.isDisplayed).toBeTruthy();
            dashboardsPage.widgetGalleryCrossBtn.click();
            browser.waitForAngular();
        });

        afterAll(function () {
            browser.waitForAngular();
            loginPage.logout();
        });
    })