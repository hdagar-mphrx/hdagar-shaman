/**
 * Created by sujata on 10/12/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3346.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('perform search for "Partial" option ID: MI-3346', function () {
        minerva.results.btnDropdown.click();
        minerva.results.statusDropdown.click()
        minerva.results.statusDropdown.sendKeys('Cancelled');
        minerva.results.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual('Status: Cancelled')
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.noResultFound.getText()).toEqual(testData.noResultFound)
    });

    afterEach(function () {
        minerva.logout();
    });
});