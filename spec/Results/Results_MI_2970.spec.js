   var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_2970.json');

describe('results table', function() {
    beforeEach(function () {
         minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
        });
    });

    it('landing on records view ID: MI-2970', function () {
        expect(minerva.results.resultsTable.title).toEqual(testData.tabName);
    });

    afterEach(function () {
        minerva.logout();
    });
});