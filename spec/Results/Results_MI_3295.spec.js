/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3295.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('search by valid patId ID: MI-3295', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputPatientIdAdvance.sendKeys(testData.patientId)
        minerva.results.btnAdvanceSearch.click()
        var count = parseInt(testData.patientIdSearchData[9]);
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient ID: '+testData.patientId)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(columns['PatientName'].data()).toEqual([testData.patientIdSearchData[0]]);
        expect(columns['ValueId'].data()).toEqual([testData.patientIdSearchData[1]]);
        expect(columns['ValueBorn'].data()).toEqual([testData.patientIdSearchData[2]]);
        expect(columns['ReportType'].data()).toEqual([testData.patientIdSearchData[4]]);
        expect(columns['ReportStatus'].data()).toEqual([testData.patientIdSearchData[5]]);
        expect(columns['ReportDescription'].data()).toEqual([testData.patientIdSearchData[6]]);
        expect(columns['ReportTime'].data()).toEqual([testData.patientIdSearchData[7]]);
        expect(columns['ReportLocation'].data()).toEqual([testData.patientIdSearchData[8]]);
        expect(columns['LabelId'].data()).toEqual(Array(count).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(count).fill(testData.BornText));
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient ID: '+testData.patientId)
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+count);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.patientIdSearchData[9]);
    });

    afterEach(function () {
        minerva.logout();
	});
});