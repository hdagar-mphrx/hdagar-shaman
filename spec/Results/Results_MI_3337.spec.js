/**
 * Created by sujata on 10/7/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3337.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verify test type dropdown: MI-3337', function () {
        minerva.results.btnDropdown.click();
        expect(minerva.results.recordTypeDropdownAll.getText()).toEqual('All')
        minerva.results.recordTypeDropdown.click();
        expect(minerva.results.testTypelistText).toEqual(testData.testTypeList)

    });
    afterEach(function () {
        minerva.logout();
    });
});