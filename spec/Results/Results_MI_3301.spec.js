/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData =require('./Results_MI_3301.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('invalid date in dob: MI-3301', function () {
        minerva.results.btnDropdown.click();
        minerva.results.monthDropdown.sendKeys(testData.month);
        minerva.results.Inputday.sendKeys(testData.day);
        minerva.results.Inputyear.sendKeys(testData.year)
        minerva.results.patientTextAdvance.click();
        expect(minerva.results.errorText.getText()).toEqual(testData.notValidDate)
    });
    afterEach(function () {
        minerva.logout();
	});
});