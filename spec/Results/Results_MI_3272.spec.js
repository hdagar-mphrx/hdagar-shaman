/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3272.json');

describe('results table', function(){
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
        });
    });

    it('dropdown button action ui ID: MI-3272', function () {
        minerva.results.btnDropdown.click();
        expect(minerva.results.advanceSearchContainer.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearchHeader.isDisplayed()).toBeTruthy();
        expect(minerva.results.advanceSearchHeader.getText(), testData.advanceSearchText).toBeTruthy();
        minerva.results.btnCrossAdvanceSearch.click();
    });

    afterEach(function () {
        minerva.logout();
	});
});