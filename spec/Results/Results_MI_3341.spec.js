/**
 * Created by sujata on 10/7/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3341.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verify test status dropdown: MI-3341', function () {
        minerva.results.btnDropdown.click();
        minerva.results.statusDropdown.click();
        expect(minerva.results.statusPleaseSelect.getText()).toEqual(testData.PleaseSelectStatus)
        expect(minerva.results.recordStatuslistText).toEqual(testData.testTypeList)

    });
    afterEach(function () {
        minerva.logout();
    });
});