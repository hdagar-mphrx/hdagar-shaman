/**
 * Created by sujata on 9/29/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3330.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('valid order name: MI-3330', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputOrderNameAdvance.sendKeys(testData.OrderName)
        minerva.results.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual('Order Name: '+testData.OrderName)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(columns['PatientName'].data()).toEqual(testData.orderNamePatientName);
        expect(columns['ValueId'].data()).toEqual(testData.orderNamePatientId);
        expect(columns['ValueBorn'].data()).toEqual(testData.orderNameDobs);
        expect(columns['ReportType'].data()).toEqual(testData.orderNameReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.orderNameReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.orderNameReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.orderNameReportDate);
        expect(columns['ReportLocation'].data()).toEqual([]);
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.genderImageIconOrderName);
    });
    afterEach(function () {
        minerva.logout();
	});
});