/**
 * Created by sujata on 10/13/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3347.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verify dropdown list of modality: MI-3347', function () {
        minerva.results.btnDropdown.click();
        minerva.results.modalityDropdown.click();
        expect(minerva.results.modalityPleaseSelect.getText()).toEqual(testData.pleaseSelect)
        expect(minerva.results.modalityTypelistText).toEqual(testData.modalityList)
    });

    afterEach(function () {
        minerva.logout();
    });
});