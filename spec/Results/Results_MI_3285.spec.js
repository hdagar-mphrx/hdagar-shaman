/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3285.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('patient first name search ID: MI-3285', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputPatientNameAdvance.sendKeys(testData.firstNameSearchString)
        minerva.results.btnAdvanceSearch.click()
        var count = parseInt(testData.firstNameSearchData[4]);
        console.log("total count of records ="+count);
        expect(columns['PatientName'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[0]));
        expect(columns['ValueId'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[1]));
        expect(columns['ValueBorn'].data()).toEqual(Array(count).fill(testData.firstNameSearchData[2]));
        expect(columns['ReportType'].data()).toEqual(testData.firstNameSearchReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.firstNameSearchReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.firstNameSearchReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.firstNameSearchReportDate);
        expect(columns['LabelId'].data()).toEqual(Array(count).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(count).fill(testData.BornText));
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.ImageIconName);
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportIconName);
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient Name: '+testData.firstNameSearchString)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - '+count);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.firstNameSearchData[4]);
    });
    afterEach(function () {
        minerva.logout();
	});
});