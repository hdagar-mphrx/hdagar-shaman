/**
 * Created by sujata on 9/29/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3303.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('Verification of year 0000 in dob: MI-3303', function () {
        minerva.results.btnDropdown.click();
        minerva.results.monthDropdown.sendKeys(testData.month);
        minerva.results.Inputday.clear()
        minerva.results.Inputyear.clear()
        minerva.results.Inputday.sendKeys(testData.day);
        minerva.results.Inputyear.sendKeys(testData.year)
        minerva.results.patientTextAdvance.click();
        expect(minerva.results.errorText.getText()).toEqual(testData.notValidDate)
    });
    afterEach(function () {
        minerva.logout();
	});
});