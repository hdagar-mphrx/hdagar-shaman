/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3287.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('advance search by patient first and last name ID: MI-3287', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputPatientNameAdvance.sendKeys(testData.firstLastNameString);
        minerva.results.btnAdvanceSearch.click()
        var count = parseInt(testData.firstLastNameData[4]);
        expect(columns['PatientName'].data()).toEqual(Array(count).fill(testData.firstLastNameData[0]));
        expect(columns['ValueId'].data()).toEqual(Array(count).fill(testData.firstLastNameData[1]));
        expect(columns['ValueBorn'].data()).toEqual(Array(count).fill(testData.firstLastNameData[2]));
        expect(columns['ReportType'].data()).toEqual(testData.firstLastNameReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.firstLastNameReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.firstLastNameReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.firstLastNameReportDate);
        expect(columns['LabelId'].data()).toEqual(Array(count).fill(testData.IdText));
        expect(columns['LabelBorn'].data()).toEqual(Array(count).fill(testData.BornText));
        expect(columns['ImageAvailableIcon'].data()).toEqual([false, false]);
        expect(columns['ReportLabelIcon'].data()).toEqual([true, true]);
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient Name: ' + testData.firstLastNameString)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.getCurrentResultsRange()).toEqual('1 - ' + count);
        expect(minerva.results.getTotalResultsCount()).toEqual(testData.firstLastNameData[4]);
    });
    afterEach(function () {
        minerva.logout();
    });
});