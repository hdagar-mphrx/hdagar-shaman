
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3280.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('clear all button action ID: MI-3280', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputPatientNameAdvance.sendKeys(testData.InputPatientNameAdvance)
        expect(minerva.results.InputPatientNameAdvance.getAttribute('value')).toEqual(testData.InputPatientNameAdvance)
        minerva.results.btnClearAll.click();
        expect(minerva.results.InputPatientNameAdvance.getAttribute('value')).toEqual('')
    });
    afterEach(function () {
        minerva.logout();
	});
});