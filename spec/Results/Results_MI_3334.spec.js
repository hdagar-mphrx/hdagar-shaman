/**
 * Created by sujata on 10/7/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3334.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('leading/trailing spaces  order Id: MI-3334', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputOrderIdAdvance.sendKeys('     '+testData.OrderId+'          ')
        minerva.results.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual('Order ID: '+testData.OrderId)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(columns['PatientName'].data()).toEqual(testData.orderIdPatientName);
        expect(columns['ValueId'].data()).toEqual(testData.orderIdPatientId);
        expect(columns['ValueBorn'].data()).toEqual(testData.orderIdDobs);
        expect(columns['ReportType'].data()).toEqual(testData.orderIdReportType);
        expect(columns['ReportStatus'].data()).toEqual(testData.orderIdReportStatus);
        expect(columns['ReportDescription'].data()).toEqual(testData.orderIdReportDescription);
        expect(columns['ReportTime'].data()).toEqual(testData.orderIdReportDate);
        expect(columns['ReportLocation'].data()).toEqual([]);
        expect(columns['ImageAvailableIcon'].data()).toEqual(testData.ImageIconorderId);
        expect(columns['ReportLabelIcon'].data()).toEqual(testData.ReportIconorderId);
    });
    afterEach(function () {
        minerva.logout();
    });
});