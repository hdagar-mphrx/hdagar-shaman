/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3293.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('search by non-existing patId ID: MI-3293', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputPatientIdAdvance.sendKeys(testData.nonExistingData)
        minerva.results.btnAdvanceSearch.click()
        expect(minerva.results.txtKeyword.getText()).toEqual('Patient ID: '+testData.nonExistingData)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.noResultFound.getText()).toEqual(testData.noResultsText)
    });

    afterEach(function () {
        minerva.logout();
	});
});