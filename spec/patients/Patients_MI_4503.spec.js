/**
 * Created by aayushi on 9/30/2016.
 */

    var testData=require('./Patients_MI_4503.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4503 pagination on patient view tab', function () {
        minerva.patients.btnSearch.click();
        expect(minerva.patients.getCurrentResultsRange()).toEqual(testData.currentResultsRange1);
        expect(minerva.patients.btnPreviousPage.isDisabled).toBeTruthy();
        minerva.patients.gotoNextPage();
        expect(minerva.patients.getCurrentResultsRange()).toEqual(testData.currentResultsRange2);
        minerva.patients.gotoNextPage(6);
        expect(minerva.patients.btnNextPage.isDisabled).toBeTruthy();
        minerva.patients.patientsTable.rows.then(function (rowsAll) {
            expect(rowsAll.length).toEqual(3)
        });
    });

    afterEach(function () {
        minerva.logout();
    });
});