/**
 * Created by aayushi on 10/5/2016.
 */

var testData=require('./Patients_MI_4517.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4517 enter only spaces and then search', function () {
        minerva.patients.txtName.sendKeys(testData.searchString1);
        minerva.patients.btnSearch.click();
        expect(columns['patientDetails'].data()).toEqual(testData.patientDetails);
    });

    afterEach(function () {
        minerva.logout();
    });
});