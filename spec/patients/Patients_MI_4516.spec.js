/**
 * Created by aayushi on 10/5/2016.
 */

var testData=require('./Patients_MI_4516.json'),
    minerva =require('../../pages/minerva');

describe('patients table', function() {

    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-4516 reset button action in patient view search', function () {
        minerva.patients.txtName.sendKeys(testData.searchString1);
        minerva.patients.btnSearch.click();
        minerva.patients.btnClear.click();
        minerva.patients.btnSearch.click();
        expect(columns['patientDetails'].data()).toEqual(testData.patientDetails);
    });

    afterEach(function () {
        minerva.logout();
    });
});