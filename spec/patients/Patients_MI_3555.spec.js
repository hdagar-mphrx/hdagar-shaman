/**
 * Created by aayushi on 9/30/2016.
 */

var minerva =require('../../pages/minerva');

describe('patients table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        var row, columns;
        columns = minerva.patients.patientsTable.columns;
    });

    it('MI-3555 ways to click Lab icon', function () {
        minerva.patients.patientsTable.row(1).then(function (rowTwo) {
            rowTwo.patientDetails().click();
            minerva.navbar.innerTab('Results').then(function (tab) {
                tab.visit();
            });
            expect(minerva.results.txtSearch.isDisplayed()).toBeTruthy();
            browser.navigate().back();
            browser.navigate().back();
        });

        minerva.patients.patientsTable.row(1).then(function (rowTwo) {
            rowTwo.actionsLabs().click();
            expect(minerva.results.txtSearch.isDisplayed()).toBeTruthy();
        });
    });

    afterEach(function () {
        minerva.logout();
    });
});