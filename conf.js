/**
 * Created by Ashish Raina on 9/6/2016.
 *
 * This file contains the configuration options passed to Protractor.
 * For details see Readme.md http://www.protractortest.org/#/tutorial
 *
 */

var secret = require('./secrets');
exports.config = {

    // ----- Connect to Browser Drivers ------------------------------------------
    debugMode: true,
    directConnect: true,
    seleniumAddress: 'http://localhost:4444/wd/hub',

    // ----- Tests to run --------------------------------------------------------
    // Use default globals: 'protractor', 'browser', '$', '$$', 'element', 'by'.
    // When no globals is set to true, the only global variable will be 'protractor'.
    // http://www.protractortest.org/#/api?view=ProtractorBrowser
    // http://www.protractortest.org/#/api?view=ElementArrayFinder
    noGlobals: false,

    // Spec patterns are relative to the location of this config.
    specs: [
        'spec/Results/*.js',
        /*'test/login.selfTest.js',
        'test/tabs.selfTest.js',
        'test/dashboards.selfTest.js',

        'test/results.selfTest.js'*/

	/* Dashboards Specs - by aayushi
	'spec/clinicalDashboard.spec.js',
	'spec/adminDashboards.spec.js',
	*/
    ],

    // ----- Set up browsers -----------------------------------------------------
    capabilities: {
        browserName: 'chrome',
        logName: 'Chrome - English',
        shardTestFiles: true,
        maxInstances: 5,
       //restartBrowserBetweenTests: true

    },

    params: {
        userAdmin: secret.credentialsAdmin,
        userInternal: secret.credentialsInternal
    },

    // ----- Set up Runner, reporters -----------------------------------------------------
    allScriptsTimeout: 30000,
    resultJsonOutputFile:'result.json',
    restartBrowserBetweenTests: false,

    framework: 'jasmine',

    // Options to be passed to jasmine.
    // See https://github.com/jasmine/jasmine-npm/blob/master/lib/jasmine.js for the exact options available.
    jasmineNodeOpts: {
        showColors: true,  // If true, print colors to the terminal.
        isVerbose: true,
        realtimeFailure: true,
        includeStackTrace: true,
        defaultTimeoutInterval: 45000,
        print: function() {}, // Function called to print jasmine results.
    },

    beforeLaunch: function() {},

    /* Add jasmine spec reporter
     *
     *  references:-
     *  http://jasmine.github.io/2.0/boot.html#section-Require_&amp;_Instantiate
     *  http://jasmine.github.io/2.0/boot.html#section-Reporters
     */

    onPrepare: function() {
        var SpecReporter = require('jasmine-spec-reporter');
        jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: 'all'}));
    },

/*    plugins: [{
        package: 'protractor-console',
        logLevels: ['severe']
    }],*/
};