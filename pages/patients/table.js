/**
 * Created by aayushi on 9/21/2016.
 */

var Page = require('astrolabe').Page,
    rows = require('./table/rows'),
    columns = require('./table/columns');

module.exports = Page.create({

    tblPatients: {
        get: function () {
            return $$('#patL');
        }
    },

    rows: {
        get: function () {
            return rows.all;
        }
    },

    row: {
        value: function (rowNumber) {
            return rows.byNumber(rowNumber);
        }
    },

    columns: {
        get: function () {
            return columns.all;
        }
    },

    column: {
        value: function (columnName) {
            return columns.byName(columnName);
        }
    },

    title: {
        get: function () {
            return $('.txt888');
        }
    }
});
