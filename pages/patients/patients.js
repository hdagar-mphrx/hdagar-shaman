/**
 * Created by aayushi on 9/21/2016.
 */

var Page = require('astrolabe').Page,
    basePage = require('../../pages/navbar');
    table=require('./table');

module.exports = Page.create({

    url: {value: '/#/templates'},

    patientsTable: {get: function () {return table}},

    lblTotalResultsCount: {get: function () {return $('.pager_links span b:nth-child(2)');}},

    btnClear: {get: function () {return $('[id=modfS_fClearBtn]');}},

    btnSearch: {get: function () {return $('[id=modfS_fSearchBtn]');}},

    lblName: {get: function () {return $('[id=modfS_fNameL] label');}},

    lblAge: {get: function () {return $('[id=modfS_fAgeL] label');}},

    txtName: {get: function () {return $('[id=modfS_fNameVinput]');}},

    lblPatientId: {get: function () {return $('[id=modfS_fIdL] label');}},

    txtPatientId: {get: function () {return $('[id=modfS_fIdVinput]');}},

    lblSearchBox: {get: function () {return $('[id=modfS_title]');}},

    lblEmail: {get: function () {return $('[id=modfS_fEmailL] label');}},

    txtEmail: {get: function () {return $('[id=modfS_fEmailVinput]');}},

    lblPhone: {get: function () {return $('[id=modfS_fPhoneL] label');}},

    msgNoResults: {get: function () {return $('[id=patL_noRecordsText1] h4');}},

//Variable naming convention
    txtPhone: {get: function () {return $('[id=modfS_fPhoneVinput]');}},

    ageRadioBtn: {get: function () {return $('[id=modfS_fAgeLinput]');}},

    DobRadioBtn: {get: function () {return $('[modfS_fDobLinput]');}},

    //lblAge: {get: function () {return $('[id=modfS_fAgeL] [class^=ng-b]');}},

    DobTitle: {get: function () {return $('[id=modfS_fDobL] label');}},

    fromDateInputBox: {get: function () {return $('[id=modfS_fAgeFinput]');}},

    ToDateInputBox: {get: function () {return $$('[id=modfS_fAgeTinput]');}},

    genderTitle: {get: function () {return $('[id=modfS_fGenderL] label');}},

    GenderPleaseSelectTitle: {get: function () {return $$('[data-ng-model=gender] [class^=ng]').get(0);}},

    GenderDropdownMale: {get: function () {return $$('[id=genderM]');}},

    GenderDropdownFemale: {get: function () {return $$('[id=genderF]');}},

    GenderDropdownU: {get: function () {return $$('[id=genderU]');}},


    //  pagination functions starting

    lblPageResultsRange: {get: function () {return $('.pager_links span b:nth-child(1)');}},

    eleNextPage: {get: function () {return $('.btn-group .fa-chevron-right');}},

    elePreviousPage: {get: function () {return $('.btn-group .fa-chevron-left');}}, // resolves promise, returns element!!

    btnPreviousPage: {get: function () {return this.btnFromElement(this.elePreviousPage);}}, // Constructs btn with additional properties}

    btnNextPage: {get: function () {return this.btnFromElement(this.eleNextPage);}}, // Constructs btn with additional properties

    btnFromElement: {
        value: function (btnElement) {
            var page = this;
            return {
                isDisabled: function () {
                    return page.btnIsDisabledFromElement(btnElement);
                },
            };
        }
    },

    genderFemaleText: {
        value: function () {
         return this.GenderDropdownFemale.getText().then(function (text) {
             return text[0].trim();
          });
       }
     },

    currentRangeSecondElement: {
        value: function () {
            return this.lblPageResultsRange.getText().then(function (text) {
             var index= text.indexOf("-");
                return text.substr(index + 1);
          });
       }
     },
    genderMaleText: {
        value: function () {
         return this.GenderDropdownMale.getText().then(function (text) {
             return text[0].trim();
          });
        }
     },

    genderUndifferentiatedText: {
        value: function () {
         return this.GenderDropdownU.getText().then(function (text) {
             return text[0].trim();
        });
}
},
    btnIsDisabledFromElement: {
        value: function (btnElement) {
            return btnElement.getCssValue('color').then(function (fontColor) {
                var expectedFontColor = 'rgba(152, 152, 152, 1)'; // Grey font for inactive buttons
                return (fontColor === expectedFontColor);
            });
        }
    },

    getCurrentResultsRange: {
        value: function () {
            return this.lblPageResultsRange.getText();
        }
    },

    getTotalResultsCount: {
        value: function () {
            return this.lblTotalResultsCount.getText();
        }
    },

    gotoNextPage: {
        value: function (loop) {
            loop = loop === undefined ? 1 : loop;
            while (loop) {
                this.eleNextPage.click();
                loop -= 1;
            }
        }
    },

    gotoPreviousPage: {
        value: function (loop) {
            loop = loop === undefined ? 1 : loop;
            while (loop) {
                this.elePreviousPage.click();
                loop -= 1;
            }
        }
    }

});
