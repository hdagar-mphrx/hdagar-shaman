/**
 * Created by sujata on 8/29/2016.
 */

var _ = require('lodash'),
    Page = require('astrolabe').Page;


module.exports = Page.create({

    tblRows: {
        get: function() { return $$('.clearfix.ng-scope'); }
    },

    length: {
        get: function () { return this.tblRows.length; }
    },

    all: {
        get: function () {
            var page = this;
            var rows = [];
            return this.tblRows.then(function (rowPromises) {
                _.forEach(rowPromises, function (row) {
                    //return rowPromise.then(function (row) {
                        rows.push(page.rowFromElement(row));
                    //});
                });
                return rows;
            });
        }
    },

    byNumber: {
        value: function (rowNumber) {
            // This functon is not zero-indexed (i.e., for the first row, use `getRowByNumber(1)`
            var page = this;
            return this.tblRows.then(function (resultsRows) {
                if (rowNumber < 1 || rowNumber > resultsRows.length) {
                    page.NoSuchRowException.thro('use row number 1 through ' + resultsRows.length);
                }
                return page.rowFromElement(resultsRows[rowNumber - 1]);
            });
        }
    },

    rowFromElement: {
        value: function (rowElement) {
            var page = this;
            return {
                patientName: function () { return page.rowPatientNameFromElement(rowElement); },
                labelId: function () { return page.rowLabelIDFromElement(rowElement);},
                labelBorn: function () { return page.rowLabelBornFromElement(rowElement);},
                valueId: function () { return page.rowValueIdFromElement(rowElement);},
                valueBorn: function () { return page.rowBornValueFromElement(rowElement);},
                imageIcon: function () { return page.rowImageAvailableIconFromElement(rowElement);},
                labelGender:function () { return page.rowLabelGenderFromElement(rowElement);},
                ValueGender:function () { return page.rowValueGenderFromElement(rowElement);},
            };
        }
    },

    rowPatientNameFromElement: {
        value: function (rowElement) {
            return rowElement.$('.patientDetails .fnt-size18').getText();
        }
    },

    rowLabelIDFromElement: {
        value: function (rowElement) {
            return rowElement.$('.clearfix.ng-scope .patientDetails div:nth-child(2) .col-md-4').getText();
        }
    },

    rowLabelBornFromElement: {
        value: function (rowElement) {
            return rowElement.$('.clearfix.ng-scope .patientDetails div:nth-child(3) .col-md-4').getText();
        }
    },

    rowLabelGenderFromElement: {
        value: function (rowElement) {
            return rowElement.$('.clearfix.ng-scope .patientDetails div:nth-child(4) .col-md-4').getText();
        }
    },

    rowValueGenderFromElement: {
        value: function (rowElement) {
            return rowElement.$('.clearfix.ng-scope .patientDetails div:nth-child(4) .yesWrap.col-md-8 .ng-binding').getText();
        }
    },

    rowValueIdFromElement: {
        value: function (rowElement) {
            return rowElement.$('.clearfix.ng-scope .patientDetails div:nth-child(2) .yesWrap.col-md-8').getText().then(function(str){
                return str.split(' ')[1];
            });
        }
    },

    rowBornValueFromElement: {
        value: function (rowElement) {
            return rowElement.$('.clearfix.ng-scope .patientDetails div:nth-child(3) .yesWrap.col-md-8').getText().then(function(str){
                return str.split(':')[1].trim();
            });
        }
    },

    rowReportTypeFromElement: {
        value: function (rowElement) {
            return rowElement.$('.pull-left-lg').getText().then(function(str){
                return str.split(',')[0].trim();
            });
        }
    },

    rowReportStatusFromElement: {
        value: function (rowElement) {
            return rowElement.$('.pull-left-lg').getText().then(function(str){
                return str.split(',')[1].trim();
            });
        }
    },

    rowReportDescriptionFromElement: {
        value: function (rowElement) {
            return rowElement.$('.recordDescription span').getText();
        }
    },

    rowReportTimeFromElement: {
        value: function (rowElement) {
            return rowElement.$('.pull-right-lg').getText().then(function(str){
                return str.split('at')[0].trim();
            });
        }
    },

    rowReportLocationFromElement: {
        value: function (rowElement) {
            return rowElement.$('.pull-right-lg').getText().then(function(str){
                if(str.includes('at'))
                return str.split('at')[1].trim();

            });
        }
    },

    rowReportAvailableIconFromElement: {

        value: function (rowElement) {
            var ele=rowElement.$('.reportAvailableIcon');
            return ele.isPresent().then(function (x) {
                        if(x)
                        {
                            return ele.isDisplayed();
                        }
                        else {
                            return false;
                        }
            });

        }
    },

    rowImageAvailableIconFromElement: {
        value: function (rowElement) {
            var ele=rowElement.$('.imagesAvailableIcon');
            return ele.isPresent().then(function (x) {
                if(x)
                {
                    return ele.isDisplayed();
                }
                else {
                    return false;
                }
            });
        }
    },

});

