/**
 * Created by sujata on 8/29/2016.
 */

var _ = require('lodash'),
    Page = require('astrolabe').Page,
    rows = require('./rows');

module.exports = Page.create({

    all: {
        get: function () {
            var page = this;
            var columns = {};
            var tblColumnHeadings= ['PatientName','LabelId','LabelBorn','ValueId','ValueBorn','ReportType','ReportStatus','ReportDescription','ReportTime','ReportLocation','ReportLabelIcon','ImageAvailableIcon','ValueGender','labelGender']
            tblColumnHeadings.forEach(function(headingElement){
                columns[headingElement]=page.byName(headingElement)
            })
            return columns
        }
    },

    byName: {
        value: function (columnName) {
            var page = this;
            return {
                name: function () { return columnName; },
                data: function () { return page.columnDataFromName(columnName); }
            };
        }
    },


    columnDataFromName: {

        value: function (columnName) {

            switch (columnName) {
                case "PatientName":
                    return this.PatientName;
                case "LabelId":
                    return this.LabelId;
                case "LabelBorn":
                    return this.LabelBorn;
                case "ValueId":
                    return this.ValueId;
                case "ValueBorn":
                    return this.ValueBorn;
                case "ReportType":
                    return this.ReportType;
                case "ReportStatus":
                    return this.ReportStatus;
                case "ReportDescription":
                    return this.ReportDescription;
                case "ReportTime":
                    return this.ReportTime;
                case "ReportLocation":
                    return this.ReportLocation;
                case "ReportLabelIcon":
                    return this.ReportLabelIcon;
                case "ImageAvailableIcon":
                    return this.ImageAvailableIcon;
                case "labelGender":
                    return this.labelGender;
                case "ValueGender":
                    return this.ValueGender;
            }
        }
    },

    PatientName: {
        get: function () {
            var PatientName = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowPatientNameFromElement(rowElement).then(function (patientName) {
                        PatientName.push(patientName);

                    });
                });
                return PatientName;
            });
        }
    },


    LabelId: {
        get: function () {
            var LabelId = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowLabelIDFromElement(rowElement).then(function (labelId) {
                        LabelId.push(labelId);

                    });
                });
                return LabelId;
            });
        }
    },
    LabelBorn: {
        get: function () {
            var LabelBorn = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowLabelBornFromElement(rowElement).then(function (labelBorn) {
                        LabelBorn.push(labelBorn);

                    });
                });
                return LabelBorn;
            });
        }
    },

    ValueId: {
        get: function () {
            var ValueId = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowValueIdFromElement(rowElement).then(function (valueId) {
                        ValueId.push(valueId);

                    });
                });
                return ValueId;
            });
        }
    },
    ValueBorn: {
        get: function () {
            var ValueBorn = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowBornValueFromElement(rowElement).then(function (valueBorn) {
                        ValueBorn.push(valueBorn);

                    });
                });
                return ValueBorn;
            });
        }
    },
    ReportType: {
        get: function () {
            var ReportType = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowReportTypeFromElement(rowElement).then(function (reportType) {
                        ReportType.push(reportType);

                    });
                });
                return ReportType;
            });
        }
    },
     ReportStatus: {
        get: function () {
            var ReportStatus = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowReportStatusFromElement(rowElement).then(function (reportStatus) {
                        ReportStatus.push(reportStatus);

                    });
                });
                return ReportStatus;
            });
        }
    },

    ReportDescription :{
        get: function () {
            var ReportDescription = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowReportDescriptionFromElement(rowElement).then(function (reportDescription) {
                        ReportDescription.push(reportDescription);

                    });
                });
                return ReportDescription;
            });
        }
    },

    ReportTime:{
        get: function () {
            var ReportTime = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowReportTimeFromElement(rowElement).then(function (reportTime) {
                        ReportTime.push(reportTime);

                    });
                });
                return ReportTime;
            });
        }
    },

    ReportLocation:{
        get: function () {
            var ReportLocation = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowReportLocationFromElement(rowElement).then(function (reportLocation) {
                        if (reportLocation) ReportLocation.push(reportLocation);

                    });
                });
                return ReportLocation;
            });
        }
    },


    ReportLabelIcon:{
        get: function () {
            var ReportLabelIcon = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowReportAvailableIconFromElement(rowElement).then(function (reportLabelIcon) {
                        ReportLabelIcon.push(reportLabelIcon);

                    });
                });
                return ReportLabelIcon;
            });
        }
    },

    ImageAvailableIcon:{
        get: function () {
            var ImageAvailableIcon = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowImageAvailableIconFromElement(rowElement).then(function (imageAvailableIcon) {
                        ImageAvailableIcon.push(imageAvailableIcon);
                    });
                });
                return ImageAvailableIcon;
            });
        }
    },

    labelGender:{
        get: function () {
            var LabelGender = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowLabelGenderFromElement(rowElement).then(function (labelGender) {
                        LabelGender.push(labelGender);
                    });
                });
                return LabelGender;
            });
        }
    },
    ValueGender:{
        get: function () {
            var ValueGender = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowValueGenderFromElement(rowElement).then(function (valueGender) {
                        ValueGender.push(valueGender);
                    });
                });
                return ValueGender;
            });
        }
    },


    NoSuchColumnException: {
        get: function() { return this.exception('No such column'); }
    }

});
