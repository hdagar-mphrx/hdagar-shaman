/**
 * Created by Ashish Raina on 8/28/2016.
 */

var Page = require('astrolabe').Page;
var basePage = require('./navbar');

module.exports = Page.create({
    url: { value: '/#/auth' },
    basePage: {get: function () {return basePage}},
    /**
     * returns Protractor ($) / Webdriver(FindElement) Page UI field objects
     * @return {Object} various Page UI Field Objects
     */
    btnLogin:               { get: function () { return $('#loginButton'); }},
    btnAcceptAgreement:     { get: function () { return this.findElement(this.by.css('[ng-click="acceptAgreement()"]')); } },
    btnDeclineAgreement:    { get: function () { return $('#aggr_actionDecline'); } },
    lblLoginError:          { get: function () { return $('#login_errorMessage'); } },
    lnkForgotPassword:      { get: function () { return $('#forgotPassowordLink'); } },
    lnkCreateAccount:       { get: function () { return $('#signUpLink'); }},
    txtUsername:            { get: function () { return $('#userName'); }},
    txtPassword:            { get: function () { return $('#userPassword'); }},

    /**
     * Login function for the page object.
     * @param {string=} opt_username
     * @param {string=} opt_password
     */

    login: {
            // Like `get:` was for page fields, `value:` signifies a function for the page object.
            // This is where the arguments for your page function are supplied.
            value: function (userObject) {
                // Astrolabe provides `this.driver`, which maps to the protractor instance.
                // With this code, successful logins can be shortened to `loginPage.login();`
                username = userObject.username ||  this.driver.params.userAdmin.username;
                password = userObject.password  || this.driver.params.userAdmin.password;

                this.txtUsername.clear();
                this.txtUsername.sendKeys(username);
                this.txtPassword.clear();
                this.txtPassword.sendKeys(password);
                this.btnLogin.click();
        }
    },

    /**
     * Utility function to Goto Minerva URL, login, maximize screen and accept agreement
     * @param {String} username
     * @param {String} password
     */
    goLogin: {
        value: function (username,password) {
            this.basePage.go();
            browser.driver.manage().window().maximize();
            this.login(username,password);
            this.acceptAgreement();
        }
    },

    // Actions
    acceptAgreement:        {value: function () { this.btnAcceptAgreement.click(); }},

    // Assertions
    isAgreementDisplayed:   { value: function () { return this.btnAcceptAgreement.isDisplayed();}},
    isLoggedOut:            { value: function () { return this.btnLogin.isDisplayed();}},

    // Base actions
    logout:     { value: function () { this.basePage.logout(); }},
    goHome:     { value: function () { this.basePage.goHome();}},
    go:         { value: function () { this.basePage.go();}},

    // Base assertions
    isLoggedIn: { value: function () { return this.basePage.isLoggedIn();}}

});