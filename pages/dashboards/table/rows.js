/**
 * Created by sujata on 8/29/2016.
 * Modified by ashish on 9/2/2016.
 */

var _    = require('lodash'),
    Page = require('astrolabe').Page;

module.exports = Page.create({

    tblRows: {
        get: function() {
            return $$('.table-row .row');
        }
    },

    length: {
        get: function () {
            return this.tblRows.length;
        }
    },

    all: {
        get: function () {
            var page = this;
            var rows = [];
            return this.tblRows.then(function (rowPromises) {
                _.forEach(rowPromises, function (rowPromise) {
                    //return rowPromise.then(function (row) {
                        rows.push(page.rowFromElement(rowPromise));
                   // });
                });
                return rows;
            });
        }
    },

    byNumber: {
        value: function (rowNumber) {
            // This functon is not zero-indexed (i.e., for the first row, use `getRowByNumber(1)`
            var page = this;
            return this.tblRows.then(function (dasboardsRows) {
                if (rowNumber < 1 || rowNumber > dasboardsRows.length) {
                    page.NoSuchRowException.thro('use row number 1 through ' + dasboardsRows.length);
                }
                return page.rowFromElement(dasboardsRows[rowNumber - 1]);
            });
        }
    },

    rowFromElement: {
        value: function (rowElement) {
            var page = this;
            return {
                description: function () { return page.rowDescriptionFromElement(rowElement);},
                type: function () { return page.rowTypeFromElement(rowElement);},
                user: function () { return page.rowUserFromElement(rowElement);},
                context: function () { return page.rowContextFromElement(rowElement);},
                lastUpdatedBy: function () { return page.rowLastUpdatedByFromElement(rowElement);},
                btnAction: function () { return page.rowActionsFromElement(rowElement);}
            };
        }
    },

    rowDescriptionFromElement: {
        value: function (rowElement) {
            return rowElement.$('.shortened').getText();
        }
    },

    rowTypeFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=dashbList_listType_]').getText();
        }
    },

    rowUserFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=dashbList_listUserType_]').getText();
        }
    },

    rowContextFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=dashbList_listContext_]').getText();
        }
    },

    rowLastUpdatedByFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=dashbList_listLastUpdatedBy_]').getText();
        }
    },
    rowActionsFromElement: {
        value: function (rowElement) {
            return rowElement.$('[id^=dashbList_listActions_]').isDisplayed();
        }
    },

/*    rowActionsFromElement: {
        value: function (rowElement) {
            var page = this;
            return {
                //name:       function () { return page.tabNameFromElement(rowElement); },
                value:   function () {return rowElement.$('[id^=dashbList_listActions_]')},
                visit:      function () { rowElement.click(); }
            };

        }
    },*/

    btnAction: {
        get: function (rowElement) {
            return rowElement.$('[id^=dashbList_listActions_]');
        }
    }

});

