/**
 * Created by sujata on 8/29/2016.
 */

var _ = require('lodash'),
    Page = require('astrolabe').Page,
    rows = require('./rows');

module.exports = Page.create({

    tblColumnHeadings: {
        get: function() { return $$('#dashbList_header .row div'); }
    },

    all: {
        get: function () {
            var page = this;
            var columns = {};
            return this.tblColumnHeadings.then(function (headingElements) {
                _.forEach(headingElements, function (headingElement) {
                    return headingElement.getText().then(function (headingText) {
                        columns[headingText] = page.byName(headingText);
                    });
                });
                return columns;
            });
        }
    },

    byName: {
        value: function (columnName) {
            var page = this;
            return {
                name: function () { return columnName; },
                data: function () { return page.columnDataFromName(columnName); }
            };
        }
    },

    columnDataFromName: {
        value: function (columnName) {
            switch (columnName) {
                case "Description":
                    return this.Description;
                case "Type":
                    return this.Type;
                case "User":
                    return this.User;
                case "Context":
                    return this.Context;
                case "Last updated by":
                    return this.LastUpdatedBy;
                case "Actions":
                    return this.Actions;
                default:
                    this.NoSuchColumnException.thro(columnHeading);
            }
        }
    },

    Description: {
        get: function () {
            var Description = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowDescriptionFromElement(rowElement).then(function (description) {
                        Description.push(description);

                    });
                });
                return Description;
            });
        }
    },


    Type: {
        get: function () {
            var Type = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowTypeFromElement(rowElement).then(function (type) {
                        Type.push(type);

                    });
                });
                return Type;
            });
        }
    },
    User: {
        get: function () {
            var User = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowUserFromElement(rowElement).then(function (user) {
                        User.push(user);

                    });
                });
                return User;
            });
        }
    },

    Context: {
        get: function () {
            var Context = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowContextFromElement(rowElement).then(function (context) {
                        Context.push(context);

                    });
                });
                return Context;
            });
        }
    },
    LastUpdatedBy: {
        get: function () {
            var LastUpdatedBy = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowLastUpdatedByFromElement(rowElement).then(function (lastUpdatedBy) {
                        LastUpdatedBy.push(lastUpdatedBy);
                    });
                });
                return LastUpdatedBy;
            });
        }
    },
    Actions: {
        get: function () {
            var Actions = [];
            return rows.tblRows.then(function (rowElements) {
                _.forEach(rowElements, function (rowElement) {
                    return rows.rowActionsFromElement(rowElement).then(function (actions) {
                        Actions.push(actions);
                    });
                });
                return Actions;
            });
        }
    },

    NoSuchColumnException: {
        get: function() { return this.exception('No such column'); }
    }

});
