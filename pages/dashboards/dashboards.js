
var Page = require('astrolabe').Page,
    dashboardsTable = require('../../pages/dashboards/table'),
    basePage = require('../navbar');

module.exports = Page.create({
    basePage: {get: function () {return basePage}},
    dashboardsTable: {get: function () {return dashboardsTable}},
    btnEdit: { get: function () { return $$('[id^=dashbList_HeditBtn]').get(0); }},

    url: {value: '/#/dashboards'},
    btnSearch: {get: function () {return $('.keywordSearchButton');}},
    txtKeyword: {get: function () {return $('#dashbList_SF_keyword input');}},
    selContext: {
        get: function () {
            return $$('#dashboardContext');
        }
    },
    selUser: {
        get: function () {
            return $('#applicableUserType');
        }
    },
    lblTotalResultsCount: {
        get: function () {
            return $('.pager_links span b:nth-child(2)');
        }
    },
    lblPageResultsRange: {
        get: function () {
            return $('.pager_links span b:nth-child(1)');
        }
    },
    eleNextPage: {
        get: function () {
            return $('.btn-group .fa-chevron-right');
        }
    },
    elePreviousPage: {
        get: function () {
            return $('.btn-group .fa-chevron-left'); // resolves promise, returns element!!
            //return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));  // returns a promise
        }
    },
    widgetRefreshBtn: {
        get: function () {
            return $('#patientDashboard_RefBtn'); // resolves promise, returns element!!
            //return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));  // returns a promise
        }
    },
    widgetGalleryCrossBtn: {
        get: function () {
            return $$('.modal-header .fa'); // resolves promise, returns element!!
            //return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));  // returns a promise
        }
    },
    widgetAddBtn: {
        get: function () {
            return $('#patientDashboard_AddBtn'); // resolves promise, returns element!!
            //return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));  // returns a promise
        }
    },
    widgetAddBtnClinicalDashboard: {
        get: function () {
            return $$('#clinicalDashboard_AddBtn').get(0); // resolves promise, returns element!!
            //return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));  // returns a promise
        }
    },
    widgetAddBtnPatientDashboard: {
        get: function () {
            return $$('#patientDashboard_AddBtn').get(0); // resolves promise, returns element!!
            //return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));  // returns a promise
        }
    },
    widgetGalleryText: {
        get: function () {
            return $('.modal-header .ng-binding'); // resolves promise, returns element!!
            //return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));  // returns a promise
        }
    },
    heartRateWidgetText: {
        get: function () {
            return ($$('.Heart .ng-binding').get(0)); // resolves promise, returns element!!
            //return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));  // returns a promise
        }
    },
    btnPreviousPage: {
        get: function () {
            return this.btnFromElement(this.elePreviousPage); // Constructs btn with additional properties
        }
    },
    btnNextPage: {
        get: function () {
            return this.btnFromElement(this.eleNextPage); // Constructs btn with additional properties
        }
    },
/*
//When a function is not the property of an object, then it is invoked as a function:
    var sum = add(3, 4); // sum is 7
When a function is invoked with this pattern, this is bound to the global object.
    This was a mistake in the design of the language
*/
    btnFromElement: {
        value: function (btnElement) {
            var page = this;
            return {
                isDisabled: function () {
                    return page.btnIsDisabledFromElement(btnElement);
                },
            };
        }
    },
    btnIsDisabledFromElement: {
        value: function (btnElement) {
            return btnElement.getCssValue('color').then(function (fontColor) {
                var expectedFontColor = 'rgba(152, 152, 152, 1)'; // Grey font for inactive buttons
                return (fontColor === expectedFontColor);
            });
        }
    },
    btnSearch: {
        get: function () {
            return $('.keywordSearchButton');
        }
    },
    txtSearch: {
        get: function () {
            return $('.input-group input');
        }
    },
    resultsTable: {
        get: function () {
            return resultsTable
        }
    },
    getCurrentResultsRange: {
        value: function () {
            return this.lblPageResultsRange.getText();
        }
    },
    getTotalResultsCount: {
        value: function () {
            return this.lblTotalResultsCount.getText();
        }
    },
    isBtnPreviousPageDisabled: {
     get: function () {
     this.btnPreviousPage.getCssValue('color').then(function (fontColor) {
     var expectedFontColor = 'rgba(152, 152, 152, 1)'; // Grey font for inactive buttons
     return (fontColor === expectedFontColor);
     });
     }
     },
    gotoNextPage: {
        value: function (loop) {
            loop = loop === undefined ? 1 : loop;
            while (loop) {
                this.eleNextPage.click();
                loop -= 1;
            }
        }
    },
    gotoPreviousPage: {
        value: function (loop) {
            loop = loop === undefined ? 1 : loop;
            while (loop) {
                this.elePreviousPage.click();
                loop -= 1;
            }
        }
    },
    isSelContextDisplayed: {value: function () {return this.selContext.isDisplayed()}},
    //isSelUserDisplayed1:       { value: function () { return this.selContext.isDisplayed();}},
    isSelUserDisplayed1: {value: function () {return (true === this.selUser.isDisplayed())}},
    keywordSearchTxtBoxEnterTxt: {
        // Like `get:` was for page fields, `value:` signifies a function for the page object.
        // This is where the arguments for your page function are supplied.
        value: function (keyword) {
            this.txtKeyword.clear();
            this.txtKeyword.sendKeys(keyword);
            this.btnSearch.click();
        }
    },
    contextFilterSelectValue: {
        // Like `get:` was for page fields, `value:` signifies a function for the page object.
        // This is where the arguments for your page function are supplied.
        value: function (keyword) {
        this.selContext.click;
        this.selContext.sendKeys(keyword);
        }
    },
});
