/**
 * Self Tests to check the framework itself
 *
 * Created by Ashish Raina on 9/5/2016.
 */

var _ = require('lodash'),
    navbar = require('../pages/navbar'),
    loginPage = require('../pages/login'),
    testData = require('./results.selfTest.json'),
    resultsPage = require('../pages/results/results');

describe('Self Test: Results Page Components', function () {

   var row;
    beforeAll(function () {
        loginPage.goLogin('test_internal', 'test');
        navbar.tab('Records').then(function (recordsTab) {
            recordsTab.visit();
        });

        resultsPage.resultsTable.row(1).then(function (rowOne) {
            row = rowOne;
        });
    });

    afterAll(function () {
        navbar.logout();
        loginPage.isLoggedOut();
    });

    it('should have correct Patient Details in row1', function () {

        var patientName = testData.patientNames[0];
        expect(row.patientName()).toEqual(patientName);

        var patientID = testData.patientIds[0];
        expect(row.ID()).toEqual(patientID);

        var patientBorn = testData.patientDobs[0];
        expect(row.born()).toEqual(patientBorn);

        resultsPage.resultsTable.rows.then(function (rowsAll) {
            expect(rowsAll.length).toEqual(10)
        });

    });

    it('pagination tests', function () {

      resultsPage.resultsTable.rows.then(function (rowsAll) {
            expect(rowsAll.length).toEqual(10)
        });

        expect(resultsPage.getTotalResultsCount()).toEqual('67');
        expect(resultsPage.getCurrentResultsRange()).toEqual('1 - 10');
        expect(resultsPage.btnPreviousPage.isDisabled()).toBeTruthy();
        expect(resultsPage.btnNextPage.isDisabled()).toBeFalsy();

        resultsPage.gotoNextPage();
        expect(resultsPage.btnPreviousPage.isDisabled()).toBeFalsy();
        expect(resultsPage.getCurrentResultsRange()).toEqual('11 - 20');
        resultsPage.gotoNextPage(2);
        expect(resultsPage.getCurrentResultsRange()).toEqual('31 - 40');
        resultsPage.gotoNextPage(3);
        expect(resultsPage.getCurrentResultsRange()).toEqual('61 - 67');
        resultsPage.resultsTable.rows.then(function (rowsAll) {
            expect(rowsAll.length).toEqual(7)
        });

        expect(resultsPage.btnNextPage.isDisabled()).toBeTruthy();

        resultsPage.gotoPreviousPage();
        expect(resultsPage.btnNextPage.isDisabled()).toBeFalsy();
        expect(resultsPage.getCurrentResultsRange()).toEqual('51 - 60');
        resultsPage.gotoPreviousPage(2);
        expect(resultsPage.getCurrentResultsRange()).toEqual('31 - 40');
        resultsPage.gotoPreviousPage(3);
        expect(resultsPage.btnPreviousPage.isDisabled()).toBeTruthy();

    });


    describe('Results columns', function () {
        var columns;
        beforeAll(function () {
            columns = resultsPage.resultsTable.columns;
        });

      it('should have right columns', function () {
            expect(_.keys(columns)).toEqual(testData.columns);
        });

        it('should have correct patientName data', function () {
            expect(columns['PatientName'].data()).toEqual(testData.patientNames);
        });

        it('should have correct birth data', function () {
            expect(columns['Born'].data()).toEqual(testData.patientDobs);
        });

        it('should search by first name correctly', function () {
            resultsPage.txtSearch.sendKeys(testData.searchString);
            resultsPage.btnSearch.click();
            expect(columns['PatientName'].data()).toEqual(Array(10).fill(testData.searchResult));
        });

    });
});



