/**
 * Self Test to check the framework itself
 *
 * Created by Ashish Raina on 8/28/2016.
 */
var _ = require('lodash'),
    loginPage = require('../pages/login'),
    navbar = require('../pages/navbar');

describe('Self Tests: Navbar Tabs Count, Active, Inactive colors', function () {

    var tabs;
    beforeAll(function () {
        loginPage.goLogin();
        navbar.tabs.then(function (baseTabs) {
            tabs = baseTabs;
        });
    });

    afterAll(function () {
        navbar.logout();
        loginPage.isLoggedOut();
    });

    it('should display 7 tabs', function () {
        expect(_.size(tabs)).toEqual(7);
    });

    it('should have System Settings tab active', function () {
        //expect(tabs['System Settings'].isActive()).toBeTruthy();
         navbar.tab('System Settings').then(function (systemSettingsTab) {
            expect(systemSettingsTab.isActive()).toBeTruthy();
        });
    });

    it('should have dashboards tab active', function () {
         navbar.tab('Dashboards').then(function (dashboardsTab) {
            dashboardsTab.visit();
            expect(dashboardsTab.isActive()).toBeTruthy();
        });
    });

    it('should not have Widgets tab active', function () {
        expect(tabs['Widgets'].isActive()).toBeFalsy();
    });

});