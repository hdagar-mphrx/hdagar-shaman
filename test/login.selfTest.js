/**
 * Self Test to check the framework itself
 *
 *  The describe and it syntax is from the Jasmine framework.
 *  browser is a global created by Protractor, which is used
 *  for browser-level commands such as with browser.WaitForAngular
 */
var loginPage  = require('../pages/login.js');

describe('Self Tests: Login, Agreement, Logout tests', function() {

    it("Should login as admin", function () {
        loginPage.go();
        loginPage.login();
        expect(loginPage.isAgreementDisplayed()).toBeTruthy();
    });

    it('Should accept agreement', function () {
        loginPage.acceptAgreement()
        expect(loginPage.isLoggedIn()).toBeTruthy();
    });

    it('Should logout OK', function () {
        loginPage.logout()
        expect(loginPage.isLoggedOut()).toBeTruthy();

    });

});