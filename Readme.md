Shaman
=========

`Shaman` is Protractor based framework for functional/e2e Minerva tests.

Prerequisites
-------------
Install node (https://github.com/nodejs/node/wiki/Installation)

Installation
------------
Clone the github repository.

    git clone https://bitbucket.com/Mphrx/mphrx/shaman.git
    cd shaman
    npm install

    npm test

References
----------
By default, NPM simply installs a package under node_modules.
When you're trying to install dependencies for your app/module,
you would need to first install them, and then add them (along
with the appropriate version number) to the dependencies section
of your package.json.
 
 
Protractor is an end-to-end test framework for AngularJS applications.
https://github.com/angular/protractor/blob/master/docs/api-overview.md
http://www.protractortest.org/#/api?view=webdriver.WebDriver
http://www.protractortest.org/#/api?view=webdriver.WebElement

Protractor uses the globals: `element` and `by`, which are also created by Protractor. 
The element function is used for finding HTML elements on your webpage.
element function takes one parameter, a Locator, which describes how to find the element.
The by object creates Locators.

Ex:     //Using globals element and by
        var username =  element(by.model('username'));
        var loginTitle =element(by.id('loginTitle'));
        var password =  element(by.id('userPassword'));
        
        
Protractor exports a global function element, which takes a _Locator_ and will return an _ElementFinder_.
This function finds a single element - if you need to manipulate multiple elements, use the element.all function.

The ElementFinder has a set of action methods, such as click(), getText(), and sendKeys. 
These are the core way to interact with an element and get information back from it.



Lodash  https://lodash.com/
A JavaScript utility library delivering consistency, modularity, performance, & extras.

Astrolabe https://github.com/stuplum/astrolabe/blob/master/Readme.md
An extension for protractor that adds page objects to your functional/e2e tests.


Protractor TODO
http://www.protractortest.org/#/api?view=webdriver.WebDriver.prototype.takeScreenshot
              
              
         
      
 
 
 Protractor Styleguide
 https://github.com/CarmenPopoviciu/protractor-styleguide

 Protractor Styleguide excerpt
 Generic Protractor Rules
        [Rule-01: Do not cheat on your tests]
         [Rule-02: Prefer unit over e2e tests when possible]
         [Rule-03: Don't e2e test what has already been unit tested]
         [Rule-04: Use one configuration file]
 Project structure
         [Rule-05: Group your e2e tests in a structure that makes sense to the structure of your project]
 Locator strategies
         [Rule-06: Never use xpath]
         [Rule-07: Prefer Protractor locators when possible]
         [Rule-08: Prefer by.id and by.css when no Protractor locators are available]
         [Rule-09: Avoid text locators for text that changes frequently]
 Page objects
    [Rule-10: Use Page Objects to interact with page under test]
    [Rule-11: UpperCamelCase the names of your Page Objects]
    [Rule-12: Pick a descriptive file naming convention for your Page Object files]
    [Rule-13: Declare one Page Object per file]
    [Rule-14: Use a single module.exports at the end of the Page Object file]
    [Rule-15: Require and instantiate all the modules at the top]
    [Rule-16: Declare all public elements in the constructor]
    [Rule-17: Declare functions for operations that require more that one step]
    [Rule-18: Do not add any assertions in your Page Object definitions]
    [Rule-19: Add wrappers for directives, dialogs, and common elements]
 Test suites
    [Rule-20: Don't mock unless you need to]
    [Rule-21: Use Jasmine2]
    [Rule-22: Make your tests independent at file level]
    [Rule-23: Make your tests independent from each other]
    [Rule-24: Navigate to the page under test before each test]
    [Rule-25: Have a suite that navigates through the major routes of the app]